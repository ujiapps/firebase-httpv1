# Firebase HTTPv1 Client for PHP

This project aims to provide a client for Firebase Cloud Messaging HTTPv1 
protocol. Originally created to support sending notifications of an internal 
project.

Currently it implements a subset of the protocol and is a work in progress.

## License

This project is licensed under the EUPL-1.2 license.

## Usage

The package isn't listed at packagist yet (maybe never it'll be) so you can put
directly this repository in order to use it like this:

```json
"repositories": [
     {
         "type": "vcs",
         "url": "git@bitbucket.org:ujiapps/firebase-httpv1.git"
     }
 ],
 "require": {
     "uji/firebase-httpv1": "dev-master"
 }
```
