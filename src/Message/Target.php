<?php

namespace Uji\Firebase\Message;

/**
 * A target represents a data that has to be presente
 */
class Target implements \JsonSerializable {

    const TOKEN = "token";
    const TOPIC = "topic";
    const CONDITION = "condition";

    /**
     * The target value
     * @var string
     */
    private $type;

    /**
     * The value of the target: a device token for token, a topic for topic, etc.
     * @var string
     */
    private $target;

    /**
     * The constructor.
     *
     * @param string $type
     * @param string $target
     */
    private function __construct($type, $target) {
        $this->type = $type;
        $this->target = $target;
    }
    /**
     * Return a Target instance of type token with the target set to
     * $devicetoken.
     *
     * @param string $devicetoken
     * @return \Uji\Firebase\Target
     */
    public static function token($devicetoken) {
        return new Target(self::TOKEN, $devicetoken);
    }
    /**
     * Creates a Target instance of type topic with the target set to
     * $topic.
     *
     * @param string $topic
     * @return \Uji\Firebase\Target
     */
    public static function topic($topic) {
        return new Target(self::TOPIC, $topic);
    }
    /**
     * Creates a Target instance of type condition and the condition set to
     * $condition.
     *
     * @param string $condition
     * @return \Uji\Firebase\Target
     */
    public static function condition($condition) {
        return new Target(self::CONDITION, $condition);
    }

    /**
     * Returns the target type.
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Return the target.
     *
     * @return string
     */
    public function getTarget() {
        return $this->target;
    }

    public function jsonSerialize() {
        return [
            $this->type => $this->target
        ];
    }
}
