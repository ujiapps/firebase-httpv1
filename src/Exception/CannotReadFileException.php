<?php

namespace Uji\Firebase\Exception;

class CannotReadFileException extends \Exception {
}
