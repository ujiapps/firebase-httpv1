<?php

namespace Uji\Firebase\Message;

class Data implements \JsonSerializable {

    /**
     * A dictionary containing arbitrary key - value data.
     *
     * @var array
     */
    private $map;

    public function __construct() {
        $this->map = [];
    }

    /**
     * Add a new key / value pair.
     *
     * @param string $key
     * @param string $value
     */
    public function add($key, $value) {
        $this->map[$key] = $value;
        return $this;
    }

    /**
     * Test if data is empty.
     *
     * @return boolean
     */
    public function isEmpty() {
        return count($this->map) === 0;
    }

    public function jsonSerialize() {
        return (object) $this->map;
    }
}
