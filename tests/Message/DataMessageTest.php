<?php

namespace Uji\Firebase\Message;

use PHPUnit\Framework\TestCase;

class DataMessageTest extends TestCase {

    public function testConstructor() {
        $dataMessage = (new DataMessage())
                ->setName("aname")
                ->setTarget(Target::token("atoken"));

        $this->assertEquals("aname", $dataMessage->getName());
        $this->assertInstanceOf(Target::class, $dataMessage->getTarget());
    }

    public function testJson() {

        // Test without data.
        $dataMessage = (new DataMessage())
                ->setName("projects/testfcm-979b5/messages/msg1")
                ->setTarget(Target::token("atoken"));

        $expected = [
            "name" => "aname",
            "token" => "atoken",
        ];

        $this->assertJsonStringEqualsJsonString(json_encode((object) $expected), json_encode($dataMessage));

        // Test data.
        $dataMessage->addData("key1", "data1");

        $expected['data'] = (object) [
            "key1" => "data1"
        ];

        $this->assertJsonStringEqualsJsonString(json_encode((object) $expected), json_encode($dataMessage));
    }
}
