<?php

namespace Uji\Firebase\Message;

/**
 * A Message.
 */
abstract class Message implements \JsonSerializable {

    /**
     * The identifier of the message to be sent.
     * @var string
     */
    private $name;

    /**
     * The message's target.
     * @var Target
     */
    private $target;

    /**
     * An optional data (can be present in notification and data messages.
     *
     * @var Data
     */
    private $data;

    /**
     * The constructor. You must set the properties by using the setters.
     */
    public function __constuct() {
        $this->name = null;
        $this->target = null;
        $this->data = new Data();
    }

    /**
     * Returns the identifier of the message.
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Returns the target of the message.
     * @return Target
     */
    public function getTarget() {
        return $this->target;
    }

    /**
     * Returns the identifier of the Message.
     *
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Set's the target of the message.
     * @param Target $target
     */
    public function setTarget(Target $target) {
        $this->target = $target;
        return $this;
    }

    /**
     * Adds a new data key / value pair.
     *
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function addData($key, $value) {
        $this->data->add($key, $value);
        return $this;
    }

    public function jsonSerialize() {

        $target = (array) $this->target->jsonSerialize();

        $ret = array_merge(
            ["name" => $this->name],
            $target
        );

        if (!$this->data->isEmpty()) {
            $ret['data'] = $this->data->jsonSerialize();
        }

        return (object) $ret;
    }
}
