<?php

namespace Uji\Firebase;

use Google\Auth\Credentials\ServiceAccountCredentials;
use Uji\Firebase\Message\Message;

class Client {

    const FIREBASE_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";
    const TIMEOUT = 5.0;
    const CONNECT_TIMEOUT = 5.0;

    /**
     * The Http Client.
     *
     * @var \GuzzleHttp\Client
     */
    private $http;

    /**
     * The project's name (in the firebase console).
     *
     * @var string
     */
    private $project;

    /**
     * The firebase cloud messaging credentials as an associative array.
     *
     * @var ServiceAccountCredentials
     */
    private $credentials;

    /**
     * Just for testing puposes.
     *
     * @var boolean
     */
    private $validate_only;

    /**
     * The url to send messages
     *
     * @var string
     */
    private $url;

    /**
     * Set to true if you wish to debug the requests.
     *
     * @var boolean
     */
    private $debug;

    /**
     * Parses a service account credentials file and returns its corresponding
     * ServiceAccountCredentials object.
     *
     * @param string $credentials_file
     * @return ServiceAccountCredentials
     * @throws Exception\CannotReadFileException
     * @throws \InvalidArgumentException
     */
    public static function parseCredentialsFile($credentials_file) {
        $json = file_get_contents($credentials_file);
        if ($json === false) {
            throw new Exception\CannotReadFileException();
        }
        $json = (array) json_decode($json);
        return ServiceAccountCredentials::makeCredentials(self::FIREBASE_SCOPE, $json);
    }

    /**
     * A Firebase httpv1 Client.
     *
     * @param string $project The project's name.
     * @param ServiceAccountCredentials $credentials A file with the credentials.
     * @param boolean $validate_only A flag used to indicate that we just want to validate the work.
     */
    public function __construct($project, ServiceAccountCredentials $credentials, $validate_only = false, $debug = false) {

        $this->project = $project;
        $this->credentials = $credentials;
        $this->validate_only = $validate_only;

        $this->url = 'https://fcm.googleapis.com/v1/projects/' . $project . '/messages:send';

        $this->http = new \GuzzleHttp\Client();

        $middleware = new \Google\Auth\Middleware\AuthTokenMiddleware($credentials);
        $stack = \GuzzleHttp\HandlerStack::create();
        $stack->push($middleware);

        $this->http = new \GuzzleHttp\Client([
            'auth' => 'google_auth',
            'handler' => $stack,

            'connect_timeout' => self::CONNECT_TIMEOUT,
            'timeout' => self::TIMEOUT

        ]);

        $this->debug = $debug;
    }

    /**
     * Send a message.
     *
     * @param \Uji\Firebase\Message $message
     * @throws \GuzzleHttp\Exception\RequestException
     */
    public function send (Message $message) {

        $payload = (object) [
            'validate_only' => $this->validate_only,
            'message' => $message
        ];

        $this->http->request('POST', $this->url, [
            'debug' => $this->debug,
            'json' => $payload
        ]);
    }

    /**
     * Sets debug flag for requests.
     *
     * @param boolean $debug
     */
    function setDebug($debug) {
        $this->debug = $debug;
    }
}

