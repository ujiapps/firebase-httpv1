<?php

namespace Uji\Firebase\Message;

use PHPUnit\Framework\TestCase;

class TargetTest extends TestCase {

    public function testFactories() {
        // Token target.
        $target = Target::token("adevicetoken");
        $this->assertEquals("token", $target->getType());
        $this->assertEquals("adevicetoken", $target->getTarget());

        // Condition target.
        $target = Target::condition("acondition");
        $this->assertEquals("condition", $target->getType());
        $this->assertEquals("acondition", $target->getTarget());

        // Topic target.
        $target = Target::topic("atopic");
        $this->assertEquals("topic", $target->getType());
        $this->assertEquals("atopic", $target->getTarget());
    }

    public function testJSON() {
        // token target.
        $target = Target::token("atoken");
        $ret = json_encode($target);
        $expected = json_encode((object) [ "token" => "atoken"]);

        $this->assertJsonStringEqualsJsonString($expected, $ret);

        // condition target.
        $target = Target::condition("acondition");
        $ret = json_encode($target);
        $expected = json_encode((object) ["condition" => "acondition"]);

        $this->assertJsonStringEqualsJsonString($expected, $ret);

        // topic target.
        $target = Target::topic("atopic");
        $ret = json_encode($target);
        $expected = json_encode((object) ["topic" => "atopic"]);

        $this->assertJsonStringEqualsJsonString($expected, $ret);
    }

}
