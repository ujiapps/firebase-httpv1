<?php

namespace Uji\Firebase\Message;

use PHPUnit\Framework\TestCase;
use Uji\Firebase\Message\Target;

class NotificationTest extends TestCase {

    /**
     * Returns a notification instance for testing pourposes.
     *
     * @return NotificationMessage
     */
    private function getNotificationInstance() {
        return (new NotificationMessage())->setTitle("A notification title")
                ->setBody("A notification body")
                ->setTarget(Target::token("atoken"))
                ->setName("A notification name");
    }

    public function testConstructor() {
        // Construír una notificación.
        $notification = $this->getNotificationInstance();

        $this->assertEquals("A notification title", $notification->getTitle());
        $this->assertEquals("A notification body", $notification->getBody());
        $this->assertInstanceOf(Target::class, $notification->getTarget());
        $this->assertEquals("A notification name", $notification->getName());
    }

    public function testJson() {
        $notification = $this->getNotificationInstance();

        $expected = [
            "name" => "A notification name",
            "token" => "atoken",
            "notification" => (object) [
                "body" => "A notification body",
                "title" => "A notification title"
            ]
        ];

        $ret = json_encode($notification);

        $this->assertJsonStringEqualsJsonString(json_encode((object) $expected), $ret);

        // Now data.
        $notification->addData("key1", "value1");

        $expected['data'] = (object) [
            "key1" => "value1"
        ];

        $this->assertJsonStringEqualsJsonString(json_encode((object) $expected), json_encode($notification));
    }

}
