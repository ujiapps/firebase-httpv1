<?php

namespace Uji\Firebase\Message;

use PHPUnit\Framework\TestCase;

class DataTest extends TestCase {

    public function testEmpty() {
        $data = new Data();
        $this->assertTrue($data->isEmpty());

        $data->add("key", "value");
        $this->assertFalse($data->isEmpty());
    }

    public function testJson() {
        $data = (new Data())->add("key1", "data1")->add("key2", "data2");

        $expected = json_encode((object) [
            "key1" => "data1",
            "key2" => "data2"
        ]);

        $this->assertJsonStringEqualsJsonString($expected, json_encode($data));
    }

}
