<?php

namespace Uji\Firebase\Message;

/**
 * A notification message.
 */
class NotificationMessage extends Message implements \JsonSerializable {
    /**
     * The notification's title.
     *
     * @var string
     */
    private $title;

    /**
     * The notification's body text.
     *
     * @var string
     */
    private $body;

    public function __construct() {
        $this->title = null;
        $this->body = null;

        parent::__constuct();
    }

    public function getTitle() {
        return $this->title;
    }

    public function getBody() {
        return $this->body;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function setBody($body) {
        $this->body = $body;
        return $this;
    }

    public function jsonSerialize() {
        $message = (array) parent::jsonSerialize();
        return (object) array_merge($message, [
            "notification" => (object) [
                "title" => $this->title,
                "body" => $this->body
            ]
        ]);
    }
}
